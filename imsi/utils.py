"""
utils
=====

Utility functions used in imsi, largely for parsing json, 
and updating, searching and modifying nested python dicts.

There are also functions for interacting with the 
DATAPATH_DB database and interacting with disk directories.

"""
from datetime import datetime
import json
import collections
from collections import OrderedDict
import sqlite3
import shutil
import fnmatch
import os

def load_json(config_file):
    """Reads a json config file"""
    with open(config_file) as f:
        # Note order can be important so use ordered dicts
        config = json.load(f, object_pairs_hook=OrderedDict)
    return config

def update(d,u, verbose=False):
    """
    Recursively update a dictionary, d, with an update, u.  
    If an item appears in only one dictionary, it is included in the result.

    Inputs:
    -------
      d : dict to update
      u : dict of updates

    Returns:
    --------
      d : updated dict
    """
    #Based on:
    #https://stackoverflow.com/questions/3232943/update-value-of-a-nested-dictionary-of-varying-depth
    #Note this is the workhorse of all imsi inheritance behaviour.  
    for k, v in u.items():
        if verbose:
            print(f"Updating {k}={v}")
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, OrderedDict()), v)
        else:
            d[k] = v
    return d

def flatten(d, parent_key='', sep='_'):
    """Flatten a nested dict, using sep to join keys from levels in order"""
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.abc.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)

def recursive_lookup(key, d):
    """Find a unique key in a nested dict. Will not be graceful if there are duplicates!"""
    for k, v in d.items():
        if k == key:
            yield v
        elif isinstance(v, collections.abc.MutableMapping):
            for result in recursive_lookup(key, v):
                yield result

def delete_or_abort(path):
    """Asks a user for input to abort or delete and replace an existing directory.
    """
    choice = input(f'{path} already exists: Abort (a) or replace (r)?')
    if choice == "a":
        print( "Exiting")
        exit()
    elif choice == "r":
        print(f"Deleting {path}")
        shutil.rmtree(path)
    else:
        print("Invalid input! Choices are 'a' or 'r'")
        delete_or_abort(path)

def get_date_string():
    """Return a datestring of now time"""

    now = datetime.now() # current date and time
    return now.strftime("%Y-%m-%d %H:%M")

def combine_configs(rootpath):
    """combine all json files found recursively under rootpath into one dictionary.
       This effectively provides a dictionary-database to use.  

       Inputs:
       -------
          rootpath : str
             The path to recursively search for input files ending in .json.
             Normally this is the path to the imsi-config directory.

       Outputs:
       --------
          config: dict
             A dictionary of the contents of the json or combined json files found under rootpath.
    """
    files = []
    config = {}
    for root, dir, filenames in os.walk(rootpath):
        for f in fnmatch.filter(filenames, '*.json'):
            files.append(os.path.join(root, f))
            temp_dict = load_json(os.path.join(root, f))
            config = update(config, temp_dict)
    return config

def parse_config_inheritance(configs, selected_config, config_hierachy=None):
    """
    Parse a configuration recursively inheriting attributes from all "parents". 

    Inputs:
    -------
       configs: dict
          dictionary of all possible configurations, typically from an imsi_database.
       selected_config: str
          The configuration to choose
       config_hierachy: dict
          Typically not user specified, but used in the recursive function call to
          layer configurations in the correct order of inheritance.
    """
    # Curiously, assigning config_hierachy to an empty default
    # leads to persistent values leaking across function calls.
    if config_hierachy is None:
        config_hierachy = [] 
    config = configs[selected_config]
    config_hierachy.append(config)
    if config['inherits_from']:
        parent_config = config['inherits_from']
        if parent_config in configs.keys():
            config = parse_config_inheritance(configs=configs, selected_config=parent_config, config_hierachy=config_hierachy)
        else:
            raise NameError(f'{parent_config} not found. Check inheritance in {selected_config}')
    else:
        if len(config_hierachy)>1:
            config = {}
            config_hierachy.reverse()
            for child in config_hierachy:
                config = update(config, child)
    return config

def get_input_paths(filename, databases, file_version=None):
    """
    This is a function to resolve a required input name to a full path to the file. 

    Inputs:
    -------
       input_filename : str
          The filename to search for
       databases : list
          A list of databases to search, in order of appearence.

    Returns:
    --------
       input_file_path : str
          The full path to the relvant file 

    In this particular case, we are going to interact with an sqlite database, building on what
    existed in CCCma. However, this could be done in other ways, with a search, with a csv or
    json database, etc. The existing datapath / access model needs to be reviewed.  
    Note that where multiple files exist with the same name, but different versions, only the one
    with the highest version number is returned. The concept of file versions separate from filenames
    is an odd one, but is a legacy at CCCma.
    """ 
    # Search for filename in databases
    for database in databases:
        con = sqlite3.connect(database)
        cur = con.cursor()
        if file_version:
            con.execute("SELECT fullpath FROM datapath WHERE filename = :file AND ver = :ver", {"file": filename, "ver": file_version})
        else:
            file_version=".*"
            cur.execute("SELECT fullpath FROM datapath WHERE filename = :file ORDER BY ver DESC LIMIT 1", {"file": filename}) 
        result = cur.fetchall()
        con.close()  
        # We found the result so stop searching
        if result:
            break 
    # We have searched all provided databases and not found a result
    if not result:
        raise ValueError(f"No file found in {databases} matching filename: {filename} and version {file_version}")   
    return result[0][0]


def parse_var(s):
    """
    Parse a key, value pair, separated by '='
    That's the reverse of ShellArgs.    
    On the command line (argparse) a declaration will typically look like::
        
        foo=hello
    
    or::
    
        foo="hello world"

    Courtesy https://stackoverflow.com/questions/27146262/create-variable-key-value-pairs-with-argparse-python
    """
    items = s.split('=')
    key = items[0].strip() # we remove blanks around keys, as is logical
    if len(items) > 1:
        # rejoin the rest:
        value = '='.join(items[1:])
    return (key, value)


def parse_vars(items):
    """
    Parse a series of key-value pairs and return a dictionary
    Courtesy https://stackoverflow.com/questions/27146262/create-variable-key-value-pairs-with-argparse-python

    """
    d = {}

    if items:
        for item in items:
            key, value = parse_var(item)
            d[key] = value
    return d