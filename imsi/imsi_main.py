"""
===============================
 :mod:`imsi_main` -- IMSI main 
===============================

The `imsi_main` module contains the core imsi classes:
   - :class:`Configuration`
   - :class:`Simulation`

as well as a series of entry point functions, typically
called via the API. 

"""
import json
import collections.abc
import os
import shutil
import re
from collections import OrderedDict
import shutil
import subprocess
import pickle
import sys
from datetime import datetime
from string import Formatter

# Below are local modules
from imsi import git_tools
from imsi import nml_tools
from imsi.scheduler_tools import simulation_scheduler
from imsi.utils import delete_or_abort, get_date_string, parse_vars, load_json, update
from imsi.utils import flatten, combine_configs, parse_config_inheritance, recursive_lookup
from imsi.sequencers import assign_sequencer

"""
TODO:

- Deal with restarts more like in the traditional CanESM5 way.

- Simplify the run script in CanESM. In the imsi submission job, tack on additional known tasks, such as packing and saving.

- Options and selections
   - Add functionality to print specific configs from the CLI. Introduce functionality to show and select/apply options (see list and set).
   - Add functionality to modify things after initial setup (see: set & update & config).
   - Need to make at least some things specified as sensible defaults in the compset (e.g. start, stop, parent_runid, parent_branch_time)
     easily chanegeable by the user. Ideally, this should be done in a configurable way - i.e. not hardcoded, but set a list of fields to
     be changeable. How does this interact / relate to "run_config". 
   - Need some sequencing options (months?).
   - "sequencing" vs compsets for setting items including parent and start/stop
   - Try use "imsi set" to set parameters. Either free-form (i.e. any parameter) or at least the most used ones (i.e parent and start/stop/months)
   - Rationalize setting selections, options and parameters and in particular allow setting via file for any of them.
   - Under imsi list have a search functionality to search, with wildcards, for a parameter of interest.

- Structural / core coding issues
   - How set use "internal" imsi variables in configuration?
      - e.g. setting "cn_exp" in namelist to "$runid".

      for k in imsi_main.flatten(vars(sim), sep='/').keys():
        ...:     if 'shell' in k:
        ...:         print(k)

   - At setup
    - hashes should be recorded in the log
    - It should be an option where to place the code directory

   - User overrides
    - especially key variables like scratch and storage directory should be settable in ~/.imsi/imsi-user-config.json 

   - inheritance
    - consider an "inherits from" mapping that defines all the inheritance structure. 
    - e.g. { "inheritance": {"compset_config" : "sequencing["time"]", }} or
    - { "inheritance": {"compset_config" : {["model_config", "experiment_config", "sequencing["time"]"]}} 
    - where the latter defines a new config (compset_config) as the combination of model experiment and sequencing.


   - Maybe try to come up with a way to "apply" a defined shell function to some kind of imsi list.
     The point here is to avoid hardcoding things like "save" in the lists. Generators could help.
      - e.g. - packing of files into directories.
             - "save" of input_files

   - Within the CanESM framework, a central issue is defining some kind of global time parameters
    that are used consistently throughout components. 

   - Should things like "machines" be abstracted into their own classes? Would this provide utility or just uncessary complexity. Throught the configuration
    they are functionaly classes.

- Supported experiments / tests
   - test abrupt-4xCO2 experiment
   - Configure historical and amip experiments
   - check compiler flags for bit identity

- Testing
   - non existant but required
   - possibly use json schema to verify that a models imsi config files are valid (https://json-schema.org/)
   - ** SCHEMA can also be used for documentation of the fields. This is VITAL**
   - Try to derive other tests at the unit level as far as possible.

- Consistency:
   - Make subcomponent names in canesm imsi config structure match those used in the imsi jsons (i.e. use CanAM not atm)
   - Make sure experiments define only experiment necessary fields, whearas more general fields ie. mpi are defined at the model level.
   - In general try to make sure there is a universal method and approach of inheritance / updating
      - specifically make machine config act more like compiler config
      - Make the shell parameters file less arbitrary

- machine / run / scheduling commands
   - Generalize the submission scripting and specification of resources (partly done for slurm and pbs but can be improved)
   - Move model run command to model from machine config. In machine config include some other info like cpuspernode.
      - Note this is difficult because there is a cross dependency between model and machine
      - specify a number of cpus/node in machine_config and try to use that to compute total resource from the sum of component requests.

Introduce utils to achieve some tasks, possibly including tools to:
   - Be able to adjust counters (?) for namelist inputs
   - Retrieve input files (see: get_input_paths and also processing of input_files)
   - list queues (see: qstat)

CLEANUP / smaller items:

- rearrange ultility functions into modules and out of main here

- Updating of cpp keys including NEMO fcm files

- allow compiler flags to be set by component
   - Doesn't just using the existing FFLAG options allow this?

"""
class Configuration:
    """
    This is the top level class that creates an imsi database of all config info
    and gets the machine and compiler setup. 

    The :class:`Simulation` class inherits from `Configuration`, and thereby obtains
    machine and compiler setup.
    """
    def __init__(self, imsi_config_path, selected_machine=None, selected_compiler=None):
        self.imsi_config_path = imsi_config_path
        self.database = combine_configs(imsi_config_path)

        self._config = {"selections":{}, "options": {}}

        self.set_machine_config(selected_machine)
        self.set_compiler_config(selected_compiler)
        self.set_user_config()

    def set_machine_config(self, selected_machine):
        """
        Set the machine based on user input, or
        determine the machine from the hostname.
        """
        machine_configs = self.database['machines']
        selected_machine_config = {}

        if selected_machine:
            if selected_machine not in machine_configs.keys():
                raise NameError(f'Could not find any imsi-supportd machine matching name {selected_machine}')
        else:
            host = None
            if 'HOSTNAME' in os.environ:
                host = os.environ['HOSTNAME']
            if not host:
                raise ValueError('Could not determine the hostname')

            for k,v in machine_configs.items():
                result = re.search(v['nodename_regex'], host) # Could this be an if in .x.keys? (that would not be an regex)
                if result:
                    selected_machine = k
                    break
            if not selected_machine:
                raise NameError(f'Could not find any imsi-supported machine matching hostname {host}')

        # It might be worth extending the above to make a warning if "selected_machine" is different
        # from the actual machine that you are on. (was impossible before but is possible now)
        selected_machine_config = parse_config_inheritance(machine_configs, selected_machine)
        self.selections["machine"] = selected_machine

        # Apply the final config
        self._config["machines"] = OrderedDict({self.machine: selected_machine_config})

    # This is the getter for machine config.
    @property
    def machine(self):
        return self.selections["machine"]

    @property
    def machine_config(self):
        return self._config["machines"][self.machine]

    @property
    def selections(self):
        return self._config["selections"]

    @property
    def options(self):
        return self._config["options"]

    def set_compiler_config(self, selected_compiler):
        """
        Set compiler options, allowing machine specific additions or overwrites.
        """
        # This first step finds the right compiler config, and applies top-level
        # machine specific overwrites.
        if selected_compiler:
            if selected_compiler in self.machine_config['compilers']:
                self.selections["compiler"] = selected_compiler
            else:
                raise NameError(f'Could not find any imsi-supportd compiler matching name {selected_compiler} for machine {self.machine}')
        else:
            # Use the first one in the list as the default
            self.selections["compiler"] = self.machine_config['compilers'][0]

        compiler_configs = self.database['compilers']
        compiler_config = compiler_configs[self.compiler]
        machine_specifics = compiler_config['machine_specific']
        if self.machine in machine_specifics.keys():
            machine_specific_overrides = parse_config_inheritance(machine_specifics, self.machine)
            # Note this overrides lists, not appends them!
            compiler_config = update(compiler_config, machine_specific_overrides)
        del compiler_config['machine_specific']

      # This whole level is searching for the "APPEND" key at the flags level for a given
      # compiler option. Where append is found, the flags under the key are attempted to be
      # added to the default compiler flags with this same key, failing which we attempt to
      # add them to the default flags.

        # It is likely that this could be cleaned up, and some of the logic moved into a function
        # for repeated use, esp. the isinstance parts.
        for language, lang_config in compiler_config.items():
            if isinstance(lang_config, collections.abc.MutableMapping):
                for options, values in lang_config.items():
                    if isinstance(values, collections.abc.MutableMapping):
                        option_keys = values.keys()
                        if "APPEND" in option_keys:
                            for k,v in values['APPEND'].items():
                                if k in option_keys:
                                    values[k] += v
                                elif "DEFAULT" in option_keys:
                                    values['DEFAULT'] += v
                                else:
                                    print(f"set_compiler_config: Nowhere found to append {k}")
                            del values['APPEND']

        #self.compiler_config = compiler_config
        self._config["compilers"] = OrderedDict({self.compiler: compiler_config})

    @property
    def compiler(self):
        return self.selections["compiler"]

    @property
    def compiler_config(self):
        return self._config["compilers"][self.compiler]

    def write_computational_environment(self):
        """Writes out a file that can be sourced in a shell
           to set the computational environment for compiling and running the model.
        """
        # There is an issue with the below, in that the unordered dict can put
        # e.g. module purge after the module load sections.

        with open(f'computational_environment', 'w') as f:
            f.write("# Imsi created model environment file for compiling and running\n")
            datestr = get_date_string()
            f.write(f"# Created for {self.machine} on date: {datestr}")
            f.write("\n\n# Module definitions\n")
            # The below code combines the module actions under "all" with those under the specific
            # compiler being used and writes these out to the file.
            # It is important NOT to change the order of the keys below, as module command
            # order is important. There currently is an issue with this order changing.
            if self.compiler in self.machine_config['modules']:
                module_config_compiler = self.machine_config['modules'][self.compiler]
            else:
                module_config_compiler = {}
            module_config =  self.machine_config['modules']['all']
            if module_config:
                # Combine the lists of keys, strictly preserving the order of the
                # all keys list. Normally I would use a set() if order was not important.
                compiler_keys = list(module_config_compiler.keys())
                all_keys = list(module_config.keys())
                for k in all_keys:
                    if k in compiler_keys:
                        compiler_keys.remove(k)

                module_cmd_keys = all_keys + compiler_keys

                # Here we append the strings together.
                # Note that this is purely additive. We might consider changing the machine
                # config to have an "append" feature, for appending or overwriting, as is done
                # for the compiler_config using OrderedDicts.
                for modcmd in module_cmd_keys:
                    argstr = ""
                    if modcmd in module_config_compiler.keys():
                        argstr += " ".join(module_config_compiler[modcmd])
                    if modcmd in module_config.keys():
                        argstr += " " +" ".join(module_config[modcmd])

                    f.write(f'module {modcmd} {argstr} \n')


            f.write("\n\n # Environment variables\n")
            env_variable_config =  self.machine_config['environment_variables']['all']
            for k,v in env_variable_config.items():
                f.write(f'export {k}={v}\n')

            f.write("\n\n # Environment commands\n")
            env_command_config =  self.machine_config['environment_commands']['all']
            for k,v in env_command_config.items():
                f.write(f'{k} {v}\n')

    def write_compilation_template(self):
        """Writes out the compilation_template file
        """
        compiler_config =  self.compiler_config
        with open(f'compilation_template', 'w') as f: #_{self.machine}_{self.compiler}
            f.write("# Imsi created model environment file for compilation\n")
            datestr = get_date_string()
            f.write(f"# Created for the compiler: {self.compiler} on machine: {self.machine} on date: {datestr}")
            f.write("\n\n#Compiler flag options\n")
            for language, lang_config in compiler_config.items():
                f.write(f'\n\n#{language} options\n')
                # Basically what is happening here is we are collapsing the structure
                # below a given language (e.g. fortran). Then if a key (e.g. FFLAGS)
                # appears at the top-level, it overwrites the values for that key under "all", however
                # if it appears below an "APPEND" key, it is appended to the values under "all".
                # There might be a slicker way (reduce?), but this produces the desired behaviour.
                if isinstance(lang_config, collections.abc.MutableMapping):
                    flat_lang_config = flatten(lang_config)
                    for k,v in flat_lang_config.items():
                        knoappend = k.replace('APPEND_','')
                        if isinstance(v, list):
                            argstr=' '.join(v)
                        else:
                            argstr=v
                        f.write(f'{knoappend}={argstr}\n')

    def set_user_config(self):
        """
        Look in $HOME for a .imsi directory, and
        read settings out of a imsi-user-config.json    
        Largely placeholder functionality but could well be useful
        """
        imsi_user_config_path = os.path.join(os.environ['HOME'], '.imsi/imsi-user-config.json') 
        if os.path.exists(imsi_user_config_path):
            print('Found ~/.imsi')
            user_config = load_json(imsi_user_config_path)
            self.user_config = user_config

class Simulation(Configuration):
    """
    The `Simulation` class is the main functional class in imsi
    The Simulation class inherits from a given :class:`Configuration`, and adds to it details about 
    a given experiment and model to define a unique simulation. `Simulations` also contain a series
    of functions the create the required setup structures and input files for a model run. 

    The class relies on downstream utilities and tools, notably from :mod:`imsi.sequencers` and
    :mod:`imsi.scheduler_tools`.
    """
    def __init__(self, imsi_config_path, run_config, selected_experiment, selected_model=None,
                 selected_machine=None, selected_compiler=None, seq='iss', options=None):

        super().__init__(imsi_config_path, selected_machine, selected_compiler)
        self._config["run_config"] = run_config
        self._config["diag_config"] = self.database["diagnostics"]
        self.scheduler = simulation_scheduler(self.machine_config["batch_commands"]["scheduler"], self)
        self.seq = assign_sequencer(seq, self)
        self.seq.setup()
        self.set_compset(selected_experiment, selected_model=selected_model)
        self.set_shell_config()

    @property
    def run_config(self):
        return self._config["run_config"]
    
    @property
    def diag_config(self):
        return self._config["diag_config"]

    # This pickles the object and it can be directly reloaded as is.
    # This snapshots the objects absolute state, and is not intended to be modified
    # by users. However, the object state could be altered through the json files below.
    def write_state(self):
        with open(f'.imsi_simulation_state.pkl', 'wb') as statefile:
            pickle.dump(self, statefile, pickle.HIGHEST_PROTOCOL)

    # This saves the objects internal dictionary, that fully defines it, to a json file.
    # This enables users to inspect and possibly modify the file.
    def write_configuration(self):
        with open(f'imsi_configuration_{self.run_config["runid"]}.json', 'w') as statefile:
            json.dump(self._config, statefile, sort_keys=False, indent=2)

    # This reads a json file for an imsi configuration into the object config dictionary,
    # that fully defines all properties of the object. This exists to enable users to query
    # options.
    def load_configuration(self):
        runid=self.run_config["runid"]
        imsi_state_config_file = os.path.join("./", f'imsi_configuration_{runid}.json')
        if not os.path.exists(imsi_state_config_file):
            raise ValueError(f"\n **ERROR**: No file {imsi_state_config_file}. Are you in a top-level established imsi simulation directory?")
        else:
            config = load_json(imsi_state_config_file)
        return config

    def set_compset(self, selected_experiment, selected_model=None):
        """
        A compset represents a blend together of an experiment and a model
        configuration. The experiment configuration takes precedent.
        """
        #>>>> HERE BE DRAGONS
        # Although a compset IS fundamentally what the ultimate configuration
        # represents, it does not exist upstream in the json configuration. It's existence can be problematic
        # because its name is not known in the tree reference a priori (hence, no prgramtic
        # searching/assigning based on it). It might be
        # better to do away with compsets, and instead just for example update the
        # experiment of model. Or perhaps it is OK to have the concept of a compset, but
        # do away with the unique name from the experiment-model pair.
        #<<<<
        # First parse inheritance on the selected experiment.
        experiment = parse_config_inheritance(self.database['experiments'], selected_experiment)
        self._config["experiments"] = {selected_experiment : experiment}
        self.selections["experiment"] = selected_experiment 
        # Figure out what model to use, or if the specified model is supported.
        if selected_model:
            if selected_model in experiment['supported_models']:
                experiment['selected_model'] = selected_model
            else:
                raise NameError(f'Model {selected_model} not in the supported list of models for {selected_experiment}')
        elif len(experiment['supported_models']) > 0:
            # If no model was specified, default to the first model in the supported list
            experiment['selected_model'] = experiment['supported_models'][0]
            selected_model = experiment['selected_model']
        else:
            raise ValueError('No model specified')   

        # Now load the selected model config
        model = parse_config_inheritance(self.database['models'], selected_model)
        self._config["models"] = {selected_model : model}
        self.selections["model"] = selected_model   
        # Blend the model and experment configurations, with the experiment taking precedence
        # Note the blending is only done on the common "components" dictionary.
        components = update(model['components'], experiment['components'])
        experiment['components'] = components
        del model['components'] 
        self.selections["compset"] = f'{selected_model}_{selected_experiment}'
        self._config["compset_config"] = {self.compset : experiment}
        del self._config["experiments"] 

    def set_shell_config(self):
        """Set shell parameters based on shell_config.json and internal imsi variables"""

        # This provides a general way to create a shell parameters file, built up from variables
        # defined somewhere in the imsi configuration. Its reliance on unique keys could be problematic, and the parsing
        # might lead to issues. Works in basic testing, however the robustness will need to be assessed in the wild.
        # One obvious issue is if `shell_config.json` contains entries like "${VARIABLE}", then the processing below
        # will look for 'VARIABLE' in imsi keys and fail (or if it succeeds it will be wrong).

        raw_shell_config = self.database['shell_config'] # raw mapping of imsi to shell variables defined in shell_config.json
        
        # Do the lookup for each item.
        local_shell_config = {}
        for k, v in raw_shell_config.items():
            # Use formatter to get the names of all references in the string ({name})
            # If there a way to skip {} preceeded by "$"??? That should be done if possible
            # Maybe with a regex instead.
            used_keys = [fn for _, fn, _, _ in Formatter().parse(v) if fn is not None]
            if not used_keys:
                local_shell_config[k] = v
            else:
                # Create a dictionary, where for each key we assign the unique imsi config value
                format_dict = {}
                for key in used_keys:
                    result = set(list(recursive_lookup(key, self._config)))
                    if len(result) !=1:
                        # No results or no unique results
                        raise ValueError(f"Could not find a unique imsi definition of {v}")
                    else:
                        format_dict[key] = result.pop()
                # Now we format the original string from shell_config.json with desired values from imsi        
                local_shell_config[k] = v.format(**format_dict)
        self._config['shell_config'] = local_shell_config

    @property
    def shell_config(self):
        return self._config["shell_config"]

    def summary(self, detail=0):
        print(f"Imsi simulation summary for {self.run_config['runid']}:")   
        print("Selections:", json.dumps(self.selections, sort_keys=False, indent=2))
        print("Options: ", json.dumps(self.options, sort_keys=False, indent=2))  
        if detail==1:
            print("run_config:", json.dumps(self.run_config, sort_keys=False, indent=2))    
        if detail>1:
            print(json.dumps(self._config, sort_keys=False, indent=2))

    @property
    def experiment(self):
        return self.selections["experiment"]
    #@property
    #def experiment_config(self):
    #   return self._config["experiments"][self.experiment]

    @property
    def model(self):
        return self.selections["model"]

    @property
    def model_config(self):
        return self._config["models"][self.model]

    @property
    def compset(self):
        return self.selections["compset"]

    @property
    def compset_config(self):
        return self._config["compset_config"][self.compset]

    def setup(self, verbose=False):
        """
        Does the setup of a simulations config directory

        """
        run_config_dir = os.path.join(self.run_config['work_dir'], "config")

        if os.path.exists(run_config_dir):
            print(f"**WARNING**: config dir exists, overwritting at {run_config_dir}")
            shutil.rmtree(run_config_dir)

        os.mkdir(run_config_dir)

        # Change to config dir and setup    
        os.chdir(run_config_dir)

        # refresh the shell config
        self._config['shell_config'] = {}
        self.set_shell_config()

        # Output the computational_environment file
        self.write_computational_environment()

        # Output the compilation template file
        self.write_compilation_template()

        # Output shell parameters
        self.write_shell_parameters()

        # Output diag parameters
        self.write_diag_parameters()

        # Extract and update required files on a component by component basis
        for component, component_config in self.compset_config['components'].items():    
            os.chdir(run_config_dir)
            if verbose:
                print(f'Configuring {component}')
            os.mkdir(component)
            os.chdir(component)
            if component_config:
                component_config_path = os.path.join(self.imsi_config_path, 'models', component_config['config_dir'])
                os.mkdir('compilation')
                for k,v in component_config['cpp_defs'].items():
                    if v:
                        # Using update preserves comments in lines not altered.
                        cpp_update(os.path.join(component_config_path, k), os.path.join('compilation', os.path.basename(k)), v)
                    else:
                        # Just straight copy them
                        shutil.copyfile(os.path.join(component_config_path, k), os.path.join('compilation', os.path.basename(k)))

                os.mkdir('namelists')
                for k,v in component_config['namelists'].items():
                    if v:
                        if verbose:
                            print(f"Updating namelist: {k}")
                     
                        # Reads, update and write out namelist
                        #nml = nml_tools.nml_read(os.path.join(component_config_path, k))
                        #nml = update(nml, v)
                        #nml_tools.nml_write(os.path.join('namelists', os.path.basename(k)), nml)
                        # Using update preserves comments in lines not altered.
                        nml_tools.nml_update(os.path.join(component_config_path, k), os.path.join('namelists', os.path.basename(k)), v)
                    else:
                      # Just straight copy them
                      shutil.copyfile(os.path.join(component_config_path, k), os.path.join('namelists', os.path.basename(k)))

        # THE WRITING BELOW IS NOT DRY. COULD BE CONDENSED. 
        # This creates a shell script to get this namelist/exe and input files. It could be its own function.
        os.chdir(run_config_dir)
        with open(f'imsi_get_input_files.sh', 'a') as input_f:
            input_f.write(f"# Imsi created input file for run: {self.run_config['runid']} of experiment: {self.experiment} and component {component}\n") 
            # These are static files (i.e. exes and namelists, which live in the bin and config dirs)
            # Obtain listed executables and namelists for each component from the bin / config dirs
            # Note here were are looping to create the info, but for inputs etc we previously created
            # stand alone shell scripts. Could be done here too.
            input_f.write("# Obtain executables and namelists \n")   
            for component, component_config in self.compset_config['components'].items():
                if component_config:
                    input_f.write(f'# Obtaining {component} inputs \n')
                    # namelists
                    component_namelist_dir = os.path.join(self.run_config['work_dir'], "config", component, "namelists")
                    for namelist in component_config['namelists'].keys():
                        component_namelist_path = os.path.join(component_namelist_dir, namelist)
                        input_f.write(f"cp {component_namelist_path} . \n") 
                    # EXEs
                    component_exe_path = os.path.join(self.run_config['work_dir'], "bin", component_config["exec"])
                    input_f.write(f"cp {component_exe_path} . \n") 

                    # The are dynamic files specified in the "input files of each component"
                    input_f.write("\n# Obtain files listed in component 'input_files' config \n")
            
                    if 'input_files' in component_config.keys():
                        for k,v in component_config['input_files'].items():
                            if not v:
                                raise ValueError(f"\n\n ** No input file is specified for the required input {k} by component {component}. \n\n")    
                            input_f.write(f"access {k} {v} \n")
                            #fullpath = get_input_paths(v, databases=[self.machine_config["input_file_database"]])
                            #os.symlink(fullpath, os.path.join("inputs", k))

        # List of files to pack into directories (is this really compliant with the philosophy?)
        os.chdir(run_config_dir)
        with open(f'imsi_directory_packing.sh', 'a') as f:
            f.write(f"# Imsi created directory packing list for run: {self.run_config['runid']} of experiment: {self.experiment}  and component {component}\n")

            for component, component_config in self.compset_config['components'].items():
                if component_config:

                    if 'directory_packing' in component_config.keys():
                        f.write(f'# Directory packing for: {component} \n')
                        for k,v in component_config['directory_packing'].items():
                            f.write(f'csf_mv_files_to_dir "{v}" "{k}" \n')

        # List of files to save from TMPDIR to a more permanent location (is this really compliant with the philosophy?)
        os.chdir(run_config_dir)
        with open(f'imsi_save_output_files.sh', 'a') as f:
            f.write(f"# Imsi created output files for run: {self.run_config['runid']} of experiment: {self.experiment}  and component {component}\n")
            for component, component_config in self.compset_config['components'].items():
                if component_config:
                    if 'output_files' in component_config.keys():
                        for k,v in component_config['output_files'].items():
                            f.write(f'save {k} {v} \n')

        os.chdir(self.run_config['work_dir'])
        self.write_state()
        self.write_configuration()
        #self.write_submission_file()
        #self.write_diag_submission_file()
        self.seq.config()

        # Maybe should be a json-specified list of utility scripts that can be extracted, perhaps to bin or wrk_dir
        os.chdir(self.run_config['work_dir'])
        shutil.copy(os.path.join(self.imsi_config_path, "models", "save_restart_files.sh"), "save_restart_files.sh")
        #for k, v in self.database['utility_config'].items():
        #    shutil.copy(os.path.join(self.imsi_config_path, v), os.path.join(self.run_config['work_dir'], k))
            
    def write_shell_parameters(self):
        """
        This creates a shell_parameters file for the simulation, that contains
        variable definitions required by downstream shell scripting.

        This should be as limited as possible, since hopefully there is minimal
        downstream shell scripting. However, it is required during development
        with mixed shell-python scripting.
        """                
        # This adds selected information from each component to the list of shell parameters, include exe names and MPI sizes.
        for component, component_config in self.compset_config['components'].items():
            if "exec" in component_config.keys():
                self.shell_config[f'{component}_EXEC'] = component_config["exec"]
            if "resources" in component_config.keys():
                # assumes both mpiprocs and ompthreads are present
                self.shell_config[f'{component}_MPIPROCS'] = component_config["resources"]["mpiprocs"]
                self.shell_config[f'{component}_OMPTHREADS'] = component_config["resources"]["ompthreads"]
            if "config" in component_config.keys():
                self.shell_config[f'{component}_CONFIG'] = component_config["config"]

        shell_config =  self.shell_config
        with open(f'shell_parameters', 'w') as f: #_{self.machine}_{self.compiler}
            f.write("# Imsi created shell environment file\n")
            datestr = get_date_string()
            f.write(f"# Created for the compiler: {self.compiler} on machine: {self.machine} on date: {datestr}\n")

            for k,v in shell_config.items():
                f.write(f'export {k}={v}\n')

    def write_diag_parameters(self):
        """
        This creates a diag_parameters file for the simulation, that contains
        variable definitions required by downstream shell scripting.

        Currently this is just a pure propagation of variables from the diagnostic
        config. Ultimately, the interaction of imsi with diagnostics needs to be refined.
        """
        # We are making the hard assumption that there are not duplicated keys.
        # If there are, earlier ones will be overwritten with later ones.
        flat_diag_config = flatten(self.diag_config, sep='>')
        flat_diag_config_onekey = {}
        for k, v in flat_diag_config.items():
            key = k.split('>')[-1]
            flat_diag_config_onekey[key] = v

        print("WRITE DIAG PARAMS")
        with open(f'diag_parameters', 'w') as f: #_{self.machine}_{self.compiler}
            f.write("# Imsi created diag environment file\n")
            datestr = get_date_string()
            f.write(f"# Created for the compiler: {self.compiler} on machine: {self.machine} on date: {datestr}\n")

            for k, v in flat_diag_config_onekey.items():
                if isinstance(v, list): # normally best to avoid this to allow duck typing. Need to think more.
                    var_string = f'"{" ".join(v)}"' # This joins lists, but does nothing to existing strings. It will not work correctly 
                                                    # for a dict (but should not get one) or for an int, etc.
                else:
                    var_string = v

                f.write(f'export {k}={var_string}\n')

    def build(self):
        """
        Builds all component executables by calling an upstream script from the
        repository. 
        Then also deals with some input files, but this is basically unneeded
        """
        os.chdir(self.run_config['work_dir'])
        shutil.copy(os.path.join(self.imsi_config_path,"imsi-tmp-compile.sh"), "imsi-tmp-compile.sh")
        compile_task = subprocess.Popen(["./imsi-tmp-compile.sh"])
        compile_task.wait()
        #streamdata = compile_task.communicate()[0]
        rc = compile_task.returncode    
        if rc != 0:
            raise ValueError("Error: Compiling failed with imsi-tmp-compile")

        # Consider checking if neeeded input are available...
    def apply_options(self, selected_options):
        """
        Take a set of options or 'patches' and apply them to the simulation configuration.
        An :func:`Simulation.setup` is done so that all options are applied into the checked
        out run configuration files.

        Input:
        ------
        selected_options : dict
           k-v pairs of option name and selection
        """
        all_options = self.database['model_options']
        # Check first the option is valid
        for option, selection in selected_options.items():
            if option in all_options.keys():
                if selection in all_options[option].keys():
                    for target_config, target_values in all_options[option][selection].items():
                        # Add to list of applied options
                        self.options[option] = selection
                        # Set the options in the simulations internal state
                        this_config = getattr(self, target_config)
                        update(this_config, target_values)
                    print(f"Updated {option} with {selection}")
                else:
                    raise ValueError(f"\n**ERROR**: there is no valid selection {selection} under the option named {option}. " +
                                     f"Available selections are {list(all_options[option]['options'].keys())}")
            else:
                raise ValueError(f"\n**ERROR**: there is no option named {option}. Available options are {list(all_options.keys())}")

        # Apply all these options in the configuration on disk
        self.setup()

class Ensemble(Configuration):
    """
    This is a placeholder class to collect up multiple simulations into an ensemble.
    The order of class inheritance might need to be refactored at this time.    
    Ideally this would borrow functionality heavily from the current ensemble tool.
    """
    pass

def load_simulation():
    """Load the pickle file created at setup time, and which completely defines a simulation object"""

    if not os.path.exists(".imsi_simulation_state.pkl"):
        raise ValueError("\n\n **ERROR**: No file .imsi_simulation_state.pkl. Are you in a top level established imsi simulation directory?")

    return pickle.load(open(".imsi_simulation_state.pkl", "rb", -1))

def summarize_run(detail):
    """ Print basic output about a simulation"""
    sim = load_simulation()
    sim.summary(detail=detail)

def build_run():
    """Call the compilation function"""
    sim = load_simulation()
    sim.build()

def submit_run():
    """Instantiate the simulation object and submit job to queue"""
    sim = load_simulation()
    sim.seq.submit()

def config_run():
    sim = load_simulation()
    if os.path.exists('config'):
        shutil.rmtree('./config')
        os.mkdir('config')
    updated_config = sim.load_configuration()
    sim._config = update(sim._config, updated_config)

    print(sim.shell_config)
    sim.setup()

def update_run():
    """
    This will re-setup the simulation, re-extracting everthing out of
    the cloned repository to create the config directory.   
    I.e. if one made changes in the repo after setup, and
    wanted to apply them, they would call this update function.
    """
    sim = load_simulation()
    sim_update = Simulation(sim.imsi_config_path, sim.run_config, sim.experiment, sim.model)
    sim_update.setup()
    print(f"\nIMSI simulation update complete.\n")

def set_selections(parm_file, selections, options):
    """Parse key=value pairs of selection given on the command line
       Try to apply these to the imsi selections for the sim.
    """
    # get existing simulation config
    sim = load_simulation() 
    # parse file / parameter updates
    updated_selections = sim.selections 
    if parm_file:
        file_values = load_json(parm_file)
        # This is updating the ._config dict in place
        updated_selections = update(updated_selections, file_values)
    if selections:
        values = parse_vars(selections)
        # This is updating the ._config dict in place
        updated_selections = update(updated_selections, values)  
    #Create a new simulation that we imbue with these properties
    sim_update = Simulation(sim.imsi_config_path, sim.run_config, sim.experiment, selected_model=sim.model,
                            selected_machine=sim.machine, selected_compiler=sim.compiler, options=None) 
    # consider whether this should be a separate apply or update step.
    sim_update.setup()  
    if options:
        values = parse_vars(options)
        sim.apply_options(values)    
# These are helper functions that could possibly be moved into a utils module
# or they could be class based methods.

def setup_run(runid, repo, ver, exp, model, fetch_method, seq):
    ''' 
    Create a run directory, clone in the source code, and checkout version ver  
    
    Inputs:
    -------
    runid : str
        The name of the simulation, also used to create the directory.
    repo : str
        The path or url to the git repository to be cloned.
    ver : str
        The git treeish to checkout, typically a commit, tag or branch name
    exp : str
        Name of a valid imsi experiment to setup (required)
    model : str
        Name of a valid imsi model to use (optional)
    fetch_method: str
        How to get the source. Either "clone", "copy" or "link".
    seq : str
        Sequencer to use. Currently either "iss" or "maestro"

    Outputs:
    --------
    Returns nothing, but creates the directory named for runid and recursively
    clones the code from repo and afterwards checks out ver.
    '''
    #    ** This function should possibly be in the simulation class, possibly an option
    #       of the setup function  **   
    # Here we create a directory for the run and use this for everything.
    # Moving forward, there will need to be more configurable set of paths
    # or options for where to put things. These might be in the user config
    # or machine config, or elsewhere.
    # In terms of dealing with cloning or not, having different paths might help to
    # separate the source from the working directory.   
    if os.path.exists(runid):
        print(f'\n\n**WARNING**: The directory {runid} already exists.')
        delete_or_abort(runid)
    os.mkdir(runid)
    os.chdir(runid) 

    # Write the setup log:
    with open(".imsi-setup.log", "a") as f:
        datestr = get_date_string()
        f.write(f"IMSI setup for {runid} on {datestr}\n\n")
        f.write(' '.join(sys.argv))
        f.write("\n---\n\n") 
    cwd = os.getcwd()   
    # Clone, soft link or copy the source code
    # ** name 'canesm' is hard used for sourcedir, but could/should be relaxed? (source?)
    #    imsi should not really be specific to canesm**
    if fetch_method == 'clone':
        print(f'Cloning {ver} from {repo} for {runid}')
        git_tools.recursive_clone(repo, "canesm", ver)
    elif fetch_method == 'link':
        print(f'Soft linking source from {repo} for {runid}')
        os.symlink(repo, "canesm")
    elif fetch_method == 'copy':
        print(f'Copying source from {repo} for {runid}')
        shutil.copytree(repo, "./canesm", symlinks=True, ignore_dangling_symlinks=True)  
    src_dir = os.path.join(cwd, "canesm")
    imsi_config_path = os.path.join(src_dir, 'imsi-config') # Requirement that imsi-config appears at the highest repo level of a supported model repo.
    if not os.path.exists(imsi_config_path):
        raise ValueError("\n\n **ERROR**: 'imsi-config' directory not found at the top repo level, but is required. Is this a valid imsi configured code base?")


    os.chdir(cwd)

    run_config = {
       "runid" : runid,
       "work_dir" : cwd,
       "source_path" : src_dir,
       "source_repo" :  repo,
       "source_version" : ver,
   }

    sim = Simulation(imsi_config_path, run_config, exp, model, seq=seq)
    sim.setup()
    print(f"\nIMSI setup complete. You can now: \n\n\t\t cd {runid} \n")


def cpp_update(cpp_input_file, cpp_output_file, cpp_changes, verbose=False):
    """
    An interface function to update cpp keys.

    Inputs:
       cpp_input_file : str
          path to the cpp file to load in
       cpp_output_file : str
          path to the updated cpp file to write
       cpp_changes : dict
          key = value pairs to change in the file, where
          key appears in the default cpp file, and value is the value to replace it with

    This is a basic python implementation of the `mod_nl` routine. Ideally the work would be done by a standard
    fortran namelist parser, such as f90nml. However, no existing parsers work correctly on NEMO namelists.
    """
    # Note not yet tested on NEMO .fcm files
    with open(cpp_input_file, 'r') as infile:
        filedata = infile.read()

    with open(cpp_output_file, 'w') as outfile:
        for line in filedata.split('\n'):
            lineout = line
            if ('replace' in cpp_changes.keys()):
                for k, v in cpp_changes['replace'].items():
                    if verbose:
                        print(f'in CPP replacing {k} {v}')

                    # likely too general for fcm files, since
                    # multiple keys appear on one line.
                    lineout = re.sub(rf'.*{k}.*', f'{v}', line)

            outfile.write(f'{lineout}\n')
        if ('add' in cpp_changes.keys()):
            for k, v in cpp_changes['add'].items():
                lineout = k
                outfile.write(f'{lineout}\n')


def list_choices(repo, experiments, models, platforms, compilers, options):
    """List of supported elements in an imsi database, for example names of supported
       experiments, models, machines, compilers.
    """
    con = None

    # Users can provide a path to a imsi config anywhere on disk to parse
    # Or, we can try and load a local imsi state if it exists.
    # Should expand to using the ~/.imsi/imsi-user-config.json

    if not repo:
        if os.path.exists(".imsi_simulation_state.pkl"):
            con = load_simulation()
    else:
        if os.path.exists(repo):
            con = Configuration(repo)
    if not con:
        raise ValueError("Imsi list: You must specify a valid path to an imsi config directory (repo) or be within an imsi configured directory.")

    print(f"Options relating to imsi-config at {con.imsi_config_path}")
    print('\n')
    if experiments:
        print("Supported experiments:", ' '.join(con.database['experiments'].keys()), "\n")
    if models:
        print("Supported models:", ' '.join(con.database['models'].keys()), "\n")
    if platforms:
        print("Supported machines:",' '.join(con.database['machines'].keys()), "\n")
    if compilers:
        print("Supported compilers:", ' '.join(con.database['compilers'].keys()), "\n")
    if options:
        if options=="no_opt":
            print("Available options:")
            if con.database['model_options']:
                for k in con.database['model_options'].keys():
                    print(k,":")
                    for sk in con.database['model_options'][k]:
                        print('\t', sk)
        else:
            if con.database['model_options']:
                #for option in options:
                #print(option)
                if options in con.database['model_options'].keys():
                    print(json.dumps(con.database['model_options'][options], sort_keys=False, indent=2))





