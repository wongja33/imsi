"""
scheduler_tools
================

A module to create submission scripts and tools for different schedulers based on input directives
    
This is not the core aim of imsi, but is included for development and testing, and in particular it
enables the imsi shell sequencer (:mod:`imsi.iss`) to work across platforms.
"""
# todo


import sys
import os
import json
import subprocess
from operator import itemgetter
from functools import reduce 
from imsi.utils import get_date_string

fields = [
    ['Job_Name'],
    ['Job_Owner'],
    ['job_state'], 
    ['queue'],
    ['Resource_List', 'nodect'],
    ['Resource_List', 'walltime']
]


def pbs_q_query(user=None):
    """
    Not functional
    """
    qstring = subprocess.check_output('qstat -f -Fjson', shell=True)
    qjson = json.loads(qstring)

    for job, job_info in qjson['Jobs'].items():
        pstr=""
        vals = [job]
        for f in fields:
            vals.append(str(reduce(dict.get, f, job_info)).split('@')[0][:65] )

        # Only for running jobs do used times exist    
        if vals[3] == 'R':
            f = ['resources_used', 'walltime']
            vals.append(str(reduce(dict.get, f, job_info)).split('@')[0][:65] )
        else:
            vals.append('00:00')
            
        if user:
            if (job_info['Job_Owner'].split('@')[0] != user):
                #print(job_info['Job_Owner'].split('@')[0], user, job_info['Job_Owner'].split('@')[0]==user)
                continue
        print('{:15s} {:65s} {:8s} {:1s} {:15s} {:4s} {:10s} {:10s}'.format(*vals))

class scheduler():
    """
    A class that absracts properties for interacting  with different batch schedulers

    This is at a general level. It could be used to provide generic functionality, 
    maybe a bit like rsub or jobsub, but not quite.
    """
    def __init__(self, scheduler_name):
        self.type = scheduler_name
        print("Init shceduler")
        # Move this into a json!(?)
        self._syntax = {
            "pbs" : {
                "directive_prefix" : "#PBS",
                "submission_command" : "qsub",
                "queue_info_command" : "qstat",
                "output_redirect" : "-o {PATH}.o",
                "nominal_directives" : [
                    "-l walltime=01:00:00",
                    "-l select=1:mem=100GB"
                ]
            },
            "slurm" : {
                "directive_prefix" : "#SBATCH",
                "submission_command" : "sbatch",
                "queue_info_command" : "squeue",
                "output_redirect" : "-o {PATH}_%j.out",
                "nominal_directives" : [
                    "--time=01:00:00",
                    "--nodes=1"
                ]
            }
        }
        if scheduler_name in self._syntax.keys():
            for key, v in self._syntax[scheduler_name].items():
                setattr(self, key, v)
        else:
            raise ValueError(f"Unsupported scheduler {scheduler_name}. Supported schedulers are {','.join(list(self.syntax.keys()))}")
        

class simulation_scheduler(scheduler):
    def __init__(self, scheduler_name, simulation):
        super().__init__(scheduler_name)
        self.sim = simulation
        print("simulation sheduler hello")
        if "submit_prefix" in self.sim.machine_config['batch_commands'].keys():
            self.submission_command =  self.sim.machine_config['batch_commands']["submit_prefix"] + " " + self.submission_command
            print(f"simulation scheduler setting prefix: {self.submission_command}")

    def write_simulation_sequencing_file(self, script_name, job_identifier, user_script_to_source=None, directives=None,
                                            dependencies=None, cycle_flag="IMSI_RESUBMIT"):
        """
        Writes a generic submission file for a scheduler, in the context of an imsi simulation, with resubmission logic, relying on 
        config/.simulation.time.state time tracking.  

        Inputs:
            script_name : str
                name / path of the file to write
            job_identifier : str
                string to insert into the job name / scratch directory name
            user_script_to_source : str
                name/fullpath of the script to run
            resubmit_from : path
                A location to do the submission from
            directives : dict
                A series of k=v pairs of resources directives to set for the job.

        This is effectively a bootstraped imsi/shell sequencer. 
        Would maybe be replaced by scripting in a  real sequencer like maestro.
        However, some of the same principles / setup would apply for getting the relevant config info
        """
        if not directives:
            directives = self.nominal_directives

        listing_prefix = os.path.join(self.sim.run_config['work_dir'], 'listings', job_identifier)
        directives = directives + [self.output_redirect.format(PATH=listing_prefix) ]
        
        if os.path.exists(script_name):
            print(f"**WARNING**: Overwriting {script_name}")

        
        with open(script_name, 'w') as f:
            f.write("#!/bin/bash\n")

            # Scheduler resource directives
            for directive in directives:
               f.write(f"{self.directive_prefix} {directive}\n") 

            # Header
            f.write("\n# Imsi created shell environment file\n")
            datestr = get_date_string()
            f.write(f"# Created for the compiler: {self.sim.compiler} on machine: {self.sim.machine} for scheduler " +
                     "{self.type} on date: {datestr}\n\n")  

            # source required input information regarding env vars, settings and timers.
            f.write(f"source {self.sim.run_config['work_dir']}/config/computational_environment\n")
            f.write(f"source {self.sim.run_config['work_dir']}/config/shell_parameters\n")
            f.write(f"source {self.sim.run_config['work_dir']}/config/.simulation.time.state\n")    
            
            # Create and go to the run scratch directory
            f.write("\n# Create a scratch directory\n")
            f.write(f"IMSI_SCRATCH={self.sim.machine_config['scratch_dir']}/{self.sim.run_config['runid']}/{self.sim.run_config['runid']}_scratch_{job_identifier}_${{CURRENT_YEAR}}_$$\n")
            f.write("mkdir -p $IMSI_SCRATCH\n")
            f.write("cd $IMSI_SCRATCH\n")
            f.write("\n")   

            # Run the logic script provided 
            f.write("# User defined scripting\n")
            f.write(f"source {user_script_to_source}\n\n") 

            # Go back to the run directory
            f.write("# Wrap and job cycling\n")
            f.write(f"cd {self.sim.run_config['work_dir']}\n")

            # Resubmit if necessary
            if cycle_flag:
                timing_file=f"{self.sim.run_config['work_dir']}/config/.simulation.time.state"
                with open(timing_file, 'a') as ftime:
                    ftime.write(f"\nexport {cycle_flag}=FALSE")
                # Source timing file to determine restartability. The run script above would have altered this.
                f.write(f"source {self.sim.run_config['work_dir']}/config/.simulation.time.state\n") 
                f.write(f'if [ "${cycle_flag}" == "TRUE" ] ; then\n')
                f.write(f"   {self.submission_command} {script_name}\n")
                f.write('fi\n')

            # Submit dependencies if they exist:
            if dependencies:
                for dep in dependencies:
                    f.write(f"   {self.submission_command} {dep}\n")
